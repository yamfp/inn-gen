package inn;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class main {

	public static void main(String[] args) throws IOException {
		//�������������� ��������� �� ���������
		int range = 0;
		String type = null; 
		String filename = null;
		String kppParam = null;
		
		// ����������� ���������
		if (args.length > 0){
			range = Integer.parseInt(args[0]); 	// ���������� ��������� ���
			type = args[1]; 					// ��� ���: �� ��� ��
			filename = args[2]; 				// ��� �����, ���� ���������
			kppParam = args[3];					// �������� ��������� ���
		} else {							// �������� �� ���������, ���� ������ �� ������
			range = 1000;						// ���������� ����������� ���, �� ��������� 1000
			type = "fl";						// ��� ���, �� ��������� ��. ����� ��������� ��������: fl ��� ul
			filename = "inn.txt";				// ��� �����, �� ���������: inn.txt
			kppParam = "no";					// �������� ��������� ���: �� ��������� no. ����� ��������� ��������: no, kpprus, kppino, kpp43, kpp44, kpp45
		}
		
		// �������������� ���������� ��������� ������
		String nalogOrgan[];
		nalogOrgan = new String[90];
		
		// �������������� ��������� ���������
		Random rand = null;
		rand = new Random();
		
		// ������������ ���������� ������. ���� ��������� ����������.
		for (int i = 0; i<90; i++){
			nalogOrgan[i] = String.format("%02d", i+1);
		}
		
		int nalogOrg;
		String nalogOrgFull[];
		nalogOrgFull = new String[range];
		
		for (int i=0; i < range; i++){
			if (type.equals("fl")){		// ��������� ��� ��
				nalogOrg = rand.nextInt(89);
				nalogOrgFull[i] = nalogOrgan[nalogOrg] + String.format("%02d", rand.nextInt(100)) + String.format("%06d", rand.nextInt(999999));
			
				int[] cM11 = {7,2,4,10,3,5,9,4,6,8};
				double controlSum11r = 0;
				for (int i11 = 0;  i11 < 10; i11++){
					controlSum11r = controlSum11r + Character.getNumericValue(nalogOrgFull[i].charAt(i11)) * cM11[i11];
				}
				double controlSum11 = controlSum11r % 11;
				
				if (controlSum11 > 9){
					nalogOrgFull[i] = nalogOrgFull[i] + String.format("%01d", (int)(controlSum11 % 10));}
				else
					nalogOrgFull[i] = nalogOrgFull[i] + String.format("%01d", (int)controlSum11);
				
				int[] cM12 = {3,7,2,4,10,3,5,9,4,6,8};
				double controlSum12r = 0;
				
				for (int i12 = 0;  i12 < 11; i12++){
					controlSum12r = controlSum12r + Character.getNumericValue(nalogOrgFull[i].charAt(i12)) * cM12[i12];
				}
	
				double controlSum12 = controlSum12r % 11;			
				
				if (controlSum12 > 9){
					nalogOrgFull[i] = nalogOrgFull[i] + String.format("%01d", (int)(controlSum12 % 10));}
				else
					nalogOrgFull[i] = nalogOrgFull[i] + String.format("%01d", (int)controlSum12);
				
				System.out.println(nalogOrgFull[i]);
			} else {
				//��������� ��� ��� ��
				nalogOrg = rand.nextInt(89);
				nalogOrgFull[i] = nalogOrgan[nalogOrg] + String.format("%02d", rand.nextInt(100)) + String.format("%05d", rand.nextInt(99999));
				
				int[] cM10 = {2,4,10,3,5,9,4,6,8};
				
				double controlSum10r = 0;
				for (int i10 = 0;  i10 < 9; i10++){
					controlSum10r = controlSum10r + Character.getNumericValue(nalogOrgFull[i].charAt(i10)) * cM10[i10];
				}
				double controlSum10 = controlSum10r % 11;
				
				if (controlSum10 > 9){
					nalogOrgFull[i] = nalogOrgFull[i] + String.format("%01d", (int)(controlSum10 % 10));}
				else
					nalogOrgFull[i] = nalogOrgFull[i] + String.format("%01d", (int)controlSum10);
				
				String kpp = null;
				// ������� ���
				if (kppParam.equals("kpp")){
					String[] kppType = {"01","05","10","22","30","36","49","80"} ;
					kpp = nalogOrgan[nalogOrg] + kppType[rand.nextInt(kppType.length)] + String.format("%05d", rand.nextInt(99999));
					nalogOrgFull[i] = nalogOrgFull[i] + "/" + kpp;
				};
				
				System.out.println(nalogOrgFull[i]);
			}
			
		}
		try (FileWriter writer = new FileWriter(filename, false)){
			for (int i = 0; i < nalogOrgFull.length; i++){
				writer.write(nalogOrgFull[i]); 
				writer.append(System.lineSeparator());
		        writer.flush();
			} 
		}

	}
}
